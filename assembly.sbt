// include provided packages in runMain
runMain in Compile := Defaults
  .runMainTask(fullClasspath in Compile, runner in (Compile, run))
  .evaluated

// include provided packages in run
run in Compile := Defaults
  .runTask(fullClasspath in Compile,
           mainClass in (Compile, run),
           runner in (Compile, run))
  .evaluated

// skip packaging dependencies
assembleArtifact in assemblyPackageScala := false

assemblyMergeStrategy in assembly := {
  case PathList("org", "apache", "commons", xs @ _*) => MergeStrategy.first
  case PathList("org", "apache", "spark", "unused", xs @ _*) =>
    MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
