from os import environ

import pymysql

camapign_db_host = "performance-data.czyv6iwgcsxw.us-west-2.rds.amazonaws.com"
camapign_db_user = "sys-ai-grafana"
camapign_db_password = environ.get('CAMPAIGN_DB_PASSWORD', '')
camapign_db_name = "campaign_cache"

connection = pymysql.connect(
    host=camapign_db_host, 
    user=camapign_db_user, 
    password=camapign_db_password, 
    db=camapign_db_name
)

'''
with connection.cursor() as cursor:
    sql = "SELECT DISTINCT event_type FROM event_id;"
    cursor.execute(sql)
    connection.commit()
    event_types = [x[0] for x in cursor.fetchall() if len(x[0]) != 0]
    print(event_types)
'''

#手動從全部event_type挑出的被我認為是表達了有interest的那些
valid_event_types = {
    'app_install', 
    'app_purchase_with_revenue', 
    'app_login', 
    'app_addcart', 
    'app_open', 
    'web_submit_form', 
    'app_tutorial', 
    'app_register', 
    'app_logout', 
    'app_product', 
    'app_purchase', 
    'web_login', 
    'app_install_reengagement', 
    'web_purchase_with_revenue', 
    'web_purchase', 
    'app_booking', 
    'app_levelup', 
    'app_character', 
    'app_transaction', 
    'app_cart', 
    'app_listpage', 
    'app_submit_form', 
    'web_register', 
    'app_streaming', 
    'web_cart', 
    'video_complete', 
    'app_dating_interaction', 
    'app_search', 
    'app_wishlist', 
    'app_update', 
    'app_listing', 
    'app_deposit', 
    'app_deposite' 
}

with connection.cursor() as cursor:
    sql = "SELECT DISTINCT cid, event_id, event_type FROM event_id;"
    cursor.execute(sql)
    result = cursor.fetchall()

    allowed_event_ids_f = open("allowed_event_ids.csv", 'w', encoding='utf-8')
    allowed_event_ids_f.write("cid,event_id\n")
    for row in result:
        cid = row[0]
        event_id = row[1]
        event_type = row[2]
        if event_type in valid_event_types:
            allowed_event_ids_f.write("{},{}\n".format(cid, event_id))
    allowed_event_ids_f.close()

connection.close()
