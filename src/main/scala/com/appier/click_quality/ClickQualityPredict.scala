package com.appier.click_quality

import scala.math.sqrt

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions.rand
import org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window

import org.apache.spark.sql.types.{StringType, DoubleType}

import com.appier.sql.SQLBackend

import tw.edu.ntu.csie.liblinear._

object ClickQualityPredict {
    val logger = LogManager.getLogger(this.getClass.getSimpleName)

    val taskName = this.getClass.getSimpleName.init

    def main(args: Array[String]) {
        object conf extends ScallopConf(args) {
            val title = opt[String]()
            verify()
        }

        val optTitle = conf.title.toOption

        val title = (Option(taskName) ++ optTitle).mkString(" ")

        val sparkConf = JobUtil.defaultConf(title)
        val spark = SparkSession
            .builder()
            .config(sparkConf)
            .getOrCreate()
        import spark.implicits._

        spark.sparkContext.hadoopConfiguration.set("mapreduce.input.fileinputformat.input.dir.recursive","true")

        val imp_join_all2 = spark.read.parquet("s3a://appier-rosetta/imp_join_all/20190919/*/*")

        val imp_has_show_click = imp_join_all2
            .filter($"show_time".isNotNull)
            .filter($"click_time".isNotNull)
            .distinct() //不知道為啥會有重複的
            .cache()

        val imp_properties = imp_has_show_click
            .select(
                udfByteToStr($"appier_cookie_uid") as "appier_cookie_uid", //24
                $"show_time", //147
                $"click_time", //63
                $"age", //15
                $"amp_ad_request_type", //16
                $"androidid", //17
                udfByteToStr($"app_id") as "app_id", //19
                $"app_paid", //21
                $"browser", //37
                $"click_country", //43
                $"click_is_fraud", //61
                $"click_through_rate", //62
                udfByteToStr($"cmp_id") as "cmp_id", //65
                $"country", //69
                udfByteToStr($"crid") as "crid", //70
                $"detected_vertical_keys", //75
                $"detected_vertical_values", //76
                $"device_type", //79
                $"gender", //92
                udfByteToStr($"googleuid") as "googleuid", //93
                udfByteToStr($"idfa") as "idfa", //96
                $"imp_boxingallowed", //98
                $"imp_height", //99
                $"imp_is_instl", //100
                $"imp_position", //105
                $"imp_type", //109
                $"imp_width", //111
                $"lat", //121
                $"lon", //122
                $"os", //129
                $"osv", //130
                udfByteToStr($"page") as "page", //131
                $"partner_id", //132
                $"skippable", //148
                udfByteToStr($"tagid") as "tagid", //149
                $"timezone", //151
                $"viewability", //156
                udfByteToStr($"web_host") as "web_host", //157
                udfByteToStr($"zip") as "zip", //163
                $"click_time" - $"show_time" as "click_time_delta"
            )

        val imp_features = imp_properties
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                udfTimeDeltaCateFeature($"click_time_delta", lit("time_delta")) as "click_time_delta_f", 
                udfAppPaidCateFeature($"app_paid", lit("app_paid")) as "app_paid_f", 
                udfBrowserCateFeature($"browser", lit("browser")) as "browser_f", 
                udfDeviceTypeFeature($"device_type", lit("device_type")) as "device_type_f", 
                udfImpBoxingAllowedFeature($"imp_boxingallowed", lit("imp_boxingallowed")) as "imp_boxingallowed_f", 
                udfImpHeightFeature($"imp_height", lit("imp_height")) as "imp_height_f", 
                udfImpIsInstallFeature($"imp_is_instl", lit("imp_is_instl")) as "imp_is_instl_f", 
                udfImpPositionFeature($"imp_position", lit("imp_position")) as "imp_position_f", 
                udfImpTypeFeature($"imp_type", lit("imp_type")) as "imp_type_f", 
                udfImpWidthFeature($"imp_width", lit("imp_width")) as "imp_width_f", 
                udfOsFeature($"os", $"osv", lit("os")) as "os_f", 
                udfSkippableFeature($"skippable", lit("skippable")) as "skippable_f", 
                udfViewabilityFeature($"viewability", lit("viewability")) as "viewability_f"
            )
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                udfFlatten(
                    array(
                        $"click_time_delta_f", 
                        $"app_paid_f", 
                        $"browser_f", 
                        $"device_type_f", 
                        $"imp_boxingallowed_f", 
                        $"imp_height_f", 
                        $"imp_is_instl_f", 
                        $"imp_position_f", 
                        $"imp_type_f", 
                        $"imp_width_f", 
                        $"os_f", 
                        $"skippable_f", 
                        $"viewability_f", 
                        udfBiasFeature()
                    )
                ) as "features"
            )

        val allowed_event_ids = spark.read
            .format("csv")
            .option("header", "true")
            .load("s3a://appier-cd-imp/tmp/allowed_event_ids.csv")
            .select(
                $"cid" as "cmp_id", 
                $"event_id" as "action_id", 
                lit(true) as "is_allowed_action"
            )

        val imp_action = imp_has_show_click
            .select(
                udfByteToStr($"appier_cookie_uid") as "appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                udfByteToStr($"cmp_id") as "cmp_id",
                explode_outer($"actions") as "action" //explode_outer,沒有action會填null
            )
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                $"cmp_id",
                $"action".getItem("action_id") as "action_id"
            )

        imp_action.registerTempTable("imp_action")
        SQLBackend.register(spark.sqlContext, "SELECT appier_cookie_uid, show_time, click_time, cmp_id, action_id, isMajorAction(cmp_id, action_id) as is_major_action FROM imp_action")
        val imp_action_2 = spark.sql("SELECT appier_cookie_uid, show_time, click_time, cmp_id, action_id, isMajorAction(cmp_id, action_id) as is_major_action FROM imp_action")
            .join(allowed_event_ids, Seq("cmp_id", "action_id"), "left_outer")
            .na.fill(false, Seq("is_major_action", "is_allowed_action"))
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                udfIsValidAction($"is_major_action", $"is_allowed_action") as "is_valid_action"
            )

        val imp_actions_isvalid = imp_action_2
            .groupBy($"appier_cookie_uid", $"show_time", $"click_time")
            .agg(
                collect_set($"is_valid_action") as "is_valid_action"
            )
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                udfHasValidAction($"is_valid_action") as "has_valid_action"
            )

        val imp_label = imp_actions_isvalid
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                udfMakeLabel($"has_valid_action") as "label"
            )

        val imp_features_label = imp_label
            .join(imp_features, Seq("appier_cookie_uid", "show_time", "click_time"))

        val imp_features_label_explode = imp_features_label
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                $"label", 
                explode($"features") as "feature"
            )
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                $"label", 
                $"feature".getItem(0) as "feature_name",
                $"feature".getItem(1).cast(DoubleType) as "feature_value"
            )
            .persist(MEMORY_AND_DISK_SER)

        val feature_list = imp_features_label_explode
            .groupBy($"feature_name")
            .count()
            .select(
                $"feature_name"
            )

        val feature_index = feature_list
            .withColumn(
                "feature_index",
                row_number()
                .over(
                    Window.orderBy(
                        monotonically_increasing_id()
                    )
                ) - 1
            )

        val imp_indexed_features_label = imp_features_label_explode
            .join(feature_index, Seq("feature_name"))
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                $"label", 
                array($"feature_index".cast(StringType), $"feature_value".cast(StringType)) as "feature"
            )
            .groupBy($"appier_cookie_uid", $"show_time", $"click_time")
            .agg(
                first($"label")  as "label", 
                collect_list($"feature") as "features"
            )
            .cache()

        val imp_indexed_features_label_splits = imp_indexed_features_label
            .randomSplit(Array(0.8, 0.2))

        val train_imp_indexed_features_label = imp_indexed_features_label_splits(0)
        val test_imp_indexed_features_label = imp_indexed_features_label_splits(1)

        val train_datapoint = train_imp_indexed_features_label
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                $"label", 
                $"features"
            )
            .as[(String, Int, Int, Double, Array[Array[String]])]
            .rdd
            .map(
                dp =>
                    (
                        dp._1, 
                        dp._2, 
                        dp._3, 
                        dp._4, 
                        sparkLiblinearDataPoint(dp._4, dp._5)
                    )
            )
            .cache()
        
        val test_datapoint = test_imp_indexed_features_label
            .select(
                $"appier_cookie_uid", 
                $"show_time", 
                $"click_time", 
                $"label", 
                $"features"
            )
            .as[(String, Int, Int, Double, Array[Array[String]])]
            .rdd
            .map(
                dp =>
                    (
                        dp._1, 
                        dp._2, 
                        dp._3, 
                        dp._4, 
                        sparkLiblinearDataPoint(dp._4, dp._5)
                    )
            )
            .cache()

        val model = SparkLiblinear
            .train(
                train_datapoint.map(dp => dp._5),
                "-s 0 -c 1.0 -e 1e-2"
            )

        val predict_result = test_datapoint
            .map(
                dp =>
                    (
                        dp._1, 
                        dp._2, 
                        dp._3, 
                        dp._4, 
                        model.predict(dp._5),
                        model.predictValues(dp._5.index, dp._5.value)(0)
                    )
            )
            .toDF("appier_cookie_uid", "show_time", "click_time", "label", "predict_result", "p_value")

        val predict_result_sorted = predict_result
            .withColumn(
                "idx",
                row_number()
                    .over(
                        Window
                            .orderBy($"p_value".desc)
                    )
            )
            .cache()

        val click_count = predict_result_sorted
            .count()

        predict_result_sorted
            .filter($"idx" >= (click_count * 0).toInt)
            .groupBy($"label")
            .count()
            .show(10, false)

        predict_result_sorted
            .filter($"idx" >= (click_count * 0.25).toInt)
            .groupBy($"label")
            .count()
            .show(10, false)

        predict_result_sorted
            .filter($"idx" >= (click_count * 0.50).toInt)
            .groupBy($"label")
            .count()
            .show(10, false)
        
        predict_result_sorted
            .filter($"idx" >= (click_count * 0.75).toInt)
            .groupBy($"label")
            .count()
            .show(10, false)
    }

    val udfByteToStr = udf(byteToStr _)
    def byteToStr(bs: Array[Byte]): String = {
        encodeBase64URLSafeString(bs)
    }

    val udfHasValidAction = udf(hasValidAction _)
    def hasValidAction(is_valid_action: Seq[Boolean]): Boolean = {
        is_valid_action contains true
    }

    val udfMakeLabel = udf(makeLabel _)
    def makeLabel(has_major_action: Boolean): Double = {
        if (has_major_action) 1.0 else -1.0
    }

    val udfIsValidAction = udf(isValidAction _)
    def isValidAction(is_major_action: Boolean, is_allowed_action: Boolean): Boolean = {
        if (is_major_action && is_allowed_action) true else false
    }

    def sparkLiblinearDataPoint(label: Double, features: Array[Array[String]], instance_weight: Double = 1.0): DataPoint = {
        val features_sorted = features.sortBy(x => x(0).toInt)
        val n = features_sorted.size
        var index = new Array[Int](n)
        var value = new Array[Double](n)
        for (i <- 0 to n - 1) {
            index(i) = features_sorted(i)(0).toInt
            value(i) = features_sorted(i)(1).toDouble
        }
        new DataPoint(index, value, label, instance_weight)
    }

    val udfFlatten = udf(flatten _)
    def flatten(item_list: Seq[Seq[Seq[String]]]): Seq[Seq[String]] = {
        item_list
        .filter(x => x != null)
        .flatten
        .groupBy(_(0))
        .map {
            case (k, v) =>
            Seq(k, (v.map(x => x(1).toDouble).sum / v.length).toString)
        }
        .toSeq
    }

    val udfBiasFeature = udf(biasFeature _)
    def biasFeature(): Seq[Seq[String]] = {
        Seq[Seq[String]](Seq[String]("bias", "1.0"))
    }

    val udfTimeDeltaCateFeature = udf(timeDeltaCateFeature _)
    def timeDeltaCateFeature(time_delta: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, sqrt(time_delta).toInt), "1.0")
        )
    }

    val udfAppPaidCateFeature = udf(appPaidCateFeature _)
    def appPaidCateFeature(app_paid: Boolean, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, app_paid), "1.0")
        )
    }

    val udfBrowserCateFeature = udf(browserCateFeature _)
    def browserCateFeature(browser: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, browser), "1.0")
        )
    }

    val udfDeviceTypeFeature = udf(deviceTypeFeature _)
    def deviceTypeFeature(device_type: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, device_type), "1.0")
        )
    }

    val udfImpBoxingAllowedFeature = udf(impBoxingAllowedFeature _)
    def impBoxingAllowedFeature(imp_boxingallowed: Boolean, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, imp_boxingallowed), "1.0")
        )
    }

    val udfImpHeightFeature = udf(impHeightFeature _)
    def impHeightFeature(imp_height: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, imp_height), "1.0")
        )
    }

    val udfImpIsInstallFeature = udf(impIsInstallFeature _)
    def impIsInstallFeature(imp_is_instl: Boolean, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, imp_is_instl), "1.0")
        )
    }

    val udfImpPositionFeature = udf(impPositionFeature _)
    def impPositionFeature(imp_position: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, imp_position), "1.0")
        )
    }

    val udfImpTypeFeature = udf(impTypeFeature _)
    def impTypeFeature(imp_type: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, imp_type), "1.0")
        )
    }

    val udfImpWidthFeature = udf(impWidthFeature _)
    def impWidthFeature(imp_width: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, imp_width), "1.0")
        )
    }

    val udfOsFeature = udf(osFeature _)
    def osFeature(os: Int, osv: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s:%s".format(postfix, os, osv), "1.0")
        )
    }

    val udfSkippableFeature = udf(skippableFeature _)
    def skippableFeature(skippable: Boolean, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, skippable), "1.0")
        )
    }

    val udfViewabilityFeature = udf(viewabilityFeature _)
    def viewabilityFeature(viewability: Int, postfix: String): Seq[Seq[String]] = {
        Seq(
            Seq[String]("%s:%s".format(postfix, viewability), "1.0")
        )
    }
}
